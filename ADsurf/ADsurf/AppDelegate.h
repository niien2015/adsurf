//
//  AppDelegate.h
//  ADsurf
//
//  Created by Longfatown on 2016/11/10.
//  Copyright (c) 2016年 Longfatown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

