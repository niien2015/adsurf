//
//  HomeViewController.h
//  ADsurf
//
//  Created by Longfatown on 2016/11/10.
//  Copyright (c) 2016年 Longfatown. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface HomeViewController : BaseViewController <UITextFieldDelegate>

//
@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) IBOutlet UIButton *settingBtn;
@property (strong, nonatomic) IBOutlet UITextField *urlTextView;
@property (strong, nonatomic) IBOutlet UIButton *refreshBtn;

//
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
