//
//  HomeViewController.m
//  ADsurf
//
//  Created by Longfatown on 2016/11/10.
//  Copyright (c) 2016年 Longfatown. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.urlTextView setDelegate:self];
    [self setWebViewRequest:@"http://iddmega01.blogspot.tw"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // ⟳⟳🔄☞♆
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - Private Method
- (void)setWebViewRequest:(NSString *)urlString {
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:5.0];
    //NSURLConnection *connect = [[NSURLConnection alloc] initWithRequest:request delegate:nil];
    [self.webView loadRequest:request];
    
    [self.urlTextView setText:[self.webView.request.URL absoluteString]];
}



#pragma mark - IBAction
- (IBAction)backBtnPressed:(UIButton *)sender {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

- (IBAction)refreshBtnPressed:(UIButton *)sender {
    if (self.webView) {
        [self.webView reload];
        [self.urlTextView setText:[self.webView.request.URL absoluteString]];
    }
}

- (IBAction)settingBtnPressed:(UIButton *)sender {
    
}

#pragma mark - UITextView Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self setWebViewRequest:textField.text];
    return YES;
}

@end
